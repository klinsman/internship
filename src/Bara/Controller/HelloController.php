<?php

namespace Bara\Controller;
use Symfony\Component\HttpFoundation\Response;

/**
  *   This is controller sample for test silex konfiguration
  */

class HelloController {
    /**
      * index function
      */
    
    public function index() {
            return new Response('Message : Hello World');
        }
    
    /**
      * coba function
      */
    public function coba(){
        $coba = "test";
        echo $coba;
    }
    
    /**
      *  test function 
      */
    
    public function test(){
        
    }
    
    /**
      *  lagi function 
      */
    public function lagi(){
        
    }
    
}
