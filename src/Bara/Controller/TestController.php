<?php

namespace Bara\Controller;
use Symfony\Component\HttpFoundation\Response;

/**
  *   This is controller sample for silex
  */

class TestController{
    
    /**
      *  index function 
      */
    public function index(){
        return new Response('Message : Hello World');
    }
    
    /**
      *  function lagi 
      */
    public function lagi(){
        echo "test";
    }
    
    /**
      *  coba1 function 
      */
    public function CobaSatu(){
        $data = 'coba';
        echo $data;
    }
    
    /**
      *   coba2 function 
      */
    public function CobaDua(){
        
    }
    
    /**
      *   coba3 function 
      */
    public function CobaTiga(){
        
    }    
}