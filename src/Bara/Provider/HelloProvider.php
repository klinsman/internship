<?php

namespace Bara\Provider;

use Silex\Application;
use Silex\Api\ControllerProviderInterface;

class HelloProvider implements ControllerProviderInterface{
    
    public function connect(Application $app){
        $hello = $app ["controllers_factory"];
        $hello->get("/", "Bara\\Controller\\HelloController::index");
        
        return $hello;
    }
}