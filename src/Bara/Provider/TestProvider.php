<?php

namespace Bara\Provider;

use Silex\Application;
use Silex\Api\ControllerProviderInterface;

class TestProvider implements ControllerProviderInterface{
    
    public function connect(Application $app){
        $test = $app["controllers_factory"];
        $test->get("/", "Bara\\Controller\\TestController::index");
        
        return $test;
    }
}