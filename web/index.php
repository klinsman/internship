<?php

require_once __DIR__.'/../vendor/autoload.php';
require_once dirname(__FILE__) . '/../vendor/bshaffer/oauth2-server-php/src/OAuth2/Autoloader.php';
require_once dirname(__FILE__) . '/../vendor/swiftmailer/swiftmailer/lib/swift_required.php';


$app = new Silex\Application();
$app->register(new Silex\Provider\SwiftmailerServiceProvider());
$app->register(new Silex\Provider\ValidatorServiceProvider());
$app->register(new Silex\Provider\SessionServiceProvider());


$app['debug'];

$app->mount('/hello', new Bara\Provider\HelloProvider());
$app->mount('/test', new Bara\Provider\TestProvider());

$app->run();

